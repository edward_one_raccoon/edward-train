'use strict';

var constants = {
    SHIPPING: 'shipping',
    BILLING: 'billing',
};

module.exports = constants