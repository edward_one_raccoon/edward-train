'use strict';

var base = module.superModule;
var baseUpdateAddressFields = base.updateAddressFields;

/**
 * Copy information from address object and save it in the system
 * @param {dw.customer.CustomerAddress} newAddress - newAddress to save information into
 * @param {*} address - Address to copy from
 */
function updateAddressFields(newAddress, address) {
    baseUpdateAddressFields(newAddress, address);
    newAddress.getCustom().addressType = address.getCustom().addressType || '';
}

base.updateAddressFields = updateAddressFields;

module.exports = base;
