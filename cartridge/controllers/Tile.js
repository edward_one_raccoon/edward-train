'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Tile-Show : Used to return data for rendering a product tile
 * @name Base/Tile-Show
 * @function
 * @memberof Tile
 * @param {middleware} - cache.applyPromotionSensitiveCache
 * @param {querystringparameter} - pid - the Product ID
 * @param {querystringparameter} - ratings - boolean to determine if the reviews should be shown in the tile
 * @param {querystringparameter} - swatches - boolean to determine if the swatches should be shown in the tile
 * @param {querystringparameter} - pview - string to determine if the product factory returns a model for a tile or a pdp/quickview display
 * @param {querystringparameter} - quantity - Quantity
 * @param {querystringparameter} - dwvar_<pid>_color - Color Attribute ID
 * @param {querystringparameter} - dwvar_<pid>_size - Size Attribute ID
 * @param {category} - non-sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */
server.append('Show', function (req, res, next) {
    var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
    var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    var WishlistModel = require('*/cartridge/models/productList');
    var PAGE_SIZE_ITEMS = 15;
    var viewData = res.getViewData();

    var URLUtils = require('dw/web/URLUtils');
    var addToCartUrl = URLUtils.url('Cart-AddProduct');

    var wishlistModel = new WishlistModel(
        list,
        {
            type: 'wishlist',
            publicView: false,
            pageSize: PAGE_SIZE_ITEMS,
            pageNumber: 1
        }
    ).productList;

    var currentProductId = viewData.product.id;
    var productIsInWishlist = false;

    wishlistModel.items.forEach(function (wishlistObj) {
        if (wishlistObj.pid === currentProductId) {
            productIsInWishlist = true;
        }
    });

    viewData.addToCartUrl = addToCartUrl;
    viewData.productIsInWishlist = productIsInWishlist;

    res.setViewData(viewData);

    next();
});

module.exports = server.exports();
