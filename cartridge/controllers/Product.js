'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Product-Show : This endpoint is called to show the details of the selected product
 * @name Base/Product-Show
 * @function
 * @memberof Product
 * @param {middleware} - cache.applyPromotionSensitiveCache
 * @param {middleware} - consentTracking.consent
 * @param {querystringparameter} - pid - Product ID
 * @param {category} - non-sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */
server.append('Show', function (req, res, next) {
    var viewData = res.getViewData();
    var productListMgr = require('dw/customer/ProductListMgr');
    var collections = require('*/cartridge/scripts/util/collections');
    var counter = 0;
    var searchedProductId = viewData.product.id;

    var result = productListMgr.queryProductLists('', null);
    while (result.hasNext())
    {
        collections.forEach(result.next().items, function (item) {
            if (searchedProductId === item.productID) {
                counter++;
            }
        });
    }

    viewData.inWishlistsCount = JSON.stringify(counter);

    next();
});

module.exports = server.exports();
