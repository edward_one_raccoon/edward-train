'use strict';

var server = require('server');
server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var addressConst = require('*/cartridge/constants/Address');

/**
 * Creates a list of address model for the logged in user
 * @param {string} customerNo - customer number of the current customer
 * @returns {List} a plain list of objects of the current customer's addresses
 */
function getList(customerNo) {
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var AddressModel = require('*/cartridge/models/address');
    var collections = require('*/cartridge/scripts/util/collections');

    var customer = CustomerMgr.getCustomerByCustomerNumber(customerNo);
    var rawAddressBook = customer.addressBook.getAddresses();

    var addressBook = collections.map(rawAddressBook, function (rawAddress) {
        var addressModel = new AddressModel(rawAddress);
        addressModel.address.UUID = rawAddress.UUID;
        addressModel.address.addressType = rawAddress.getCustom().addressType || addressConst.SHIPPING;

        return addressModel;
    });

    return addressBook;
}

/**
 * Address-EditAddress : A link to edit and existing address
 * @name Base/Address-EditAddress
 * @function
 * @memberof Address
 * @param {middleware} - csrfProtection.generateToken
 * @param {middleware} - userLoggedIn.validateLoggedIn
 * @param {middleware} - consentTracking.consent
 * @param {querystringparameter} - addressId - a string used to identify the address record
 * @param {category} - sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */
server.append('EditAddress', function (req, res, next) {
        var addressId = req.querystring.addressId;
        var addressBook = customer.getProfile().getAddressBook();
        var rawAddress = addressBook.getAddress(addressId);

        var AddressModel = require('*/cartridge/models/address');
        var addressModel = new AddressModel(rawAddress);

        var viewData = res.getViewData();
        viewData.addressModel = addressModel;
        res.setViewData(viewData);

        next();
    }
);

/**
 * Address-AddAddress : A link to a page to create a new address
 * @name Base/Address-AddAddress
 * @function
 * @memberof Address
 * @param {middleware} - csrfProtection.generateToken
 * @param {middleware} - consentTracking.consent
 * @param {middleware} - userLoggedIn.validateLoggedIn
 * @param {category} - sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */
server.append(
    'AddAddress', function (req, res, next) {
        var viewData = res.getViewData();
        viewData.navTabValue = Resource.msg('link.header.shipping.header', 'forms', null);
        res.setViewData(viewData);

        next();
    }
);

/**
 * Address-List : Used to show a list of address created by a registered shopper
 * @name Base/Address-List
 * @function
 * @memberof Address
 * @param {middleware} - userLoggedIn.validateLoggedIn
 * @param {middleware} - consentTracking.consent
 * @param {category} - sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */
server.append('List', function (req, res, next) {
    var addressBook = getList(req.currentCustomer.profile.customerNo);

    var shippingAddressBook = addressBook.filter(item => item.address.addressType === addressConst.SHIPPING);
    var billingAddressBook = addressBook.filter(item => item.address.addressType === addressConst.BILLING);

    var viewData = res.getViewData();

    viewData.shippingAddressBook = shippingAddressBook;
    viewData.billingAddressBook = billingAddressBook;
    viewData.navTabValue = Resource.msg('link.header.shipping.header', 'forms', null);

    next();
});


module.exports = server.exports();
