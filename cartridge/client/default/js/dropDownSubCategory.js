'use strict';

document.body.onclick = function (e) {
    var targetEl = e.target;
    if (targetEl.className.includes('open-close-btn')) {
        e.target.parentElement.querySelector('.open-close-container').classList.toggle('d-none');
    }
}